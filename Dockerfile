# hello world
FROM busybox
RUN mkdir -p /usr/local/sbin
COPY hello /usr/local/sbin
ENV arg="world"
ENTRYPOINT hello $arg
